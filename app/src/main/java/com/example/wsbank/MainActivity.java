package com.example.wsbank;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    Button btn;
    LinearLayout courseCard, officeCard;
    TextView currentDateTV, usdTV, eurTV;
    ImageView logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Связь с разметкой
        btn = findViewById(R.id.enter_button);
        courseCard = findViewById(R.id.courses_card);
        officeCard = findViewById(R.id.office_card);
        currentDateTV = findViewById(R.id.curent_date);
        usdTV = findViewById(R.id.usd);
        eurTV = findViewById(R.id.eur);
        logo = findViewById(R.id.logo);
        //Установка даты
        currentDateTV.setText(getCurrentDate());
        //Работа с апи центробанка
        OkHttpHandler okHttpHandler= new OkHttpHandler();
        okHttpHandler.execute();
        //Обработка кнопок
        courseCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CourseActivity.class);
                startActivity(intent);
            }
        });
        officeCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, OfficeActivity.class);
                startActivity(intent);
            }
        });
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder adb = new AlertDialog.Builder(MainActivity.this);
                LayoutInflater layoutInflater = getLayoutInflater();
                View view = layoutInflater.inflate(R.layout.login, null);
                adb.setView(view);
                EditText editLogin = view.findViewById(R.id.enter_login);
                EditText editKey = view.findViewById(R.id.enter_password);
                editLogin.setText("eve.holt@reqres.in");
                editKey.setText("cityslicka");
                adb.setNeutralButton("Регистрация", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String login = editLogin.getText().toString();
                        String password = editKey.getText().toString();
                        String[] data = new String[3];
                        data[0] = login;
                        data[1] = password;
                        data[2] = null;
                        Enter enter = new Enter();
                        enter.execute(data);
                    }
                });
                adb.setPositiveButton("Вход", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String login = editLogin.getText().toString();
                        String password = editKey.getText().toString();
                        String[] data = new String[3];
                        data[0] = login;
                        data[1] = password;
                        data[2] = "login";
                        Enter enter = new Enter();
                        enter.execute(data);
                    }
                });

                AlertDialog dialog = adb.create();
                dialog.show();
            }
        });
    }

    public String getCurrentDate(){
        Date current_date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        return format.format(current_date);
    }

    public String getUrl(){
        Date current_date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return "https://www.cbr.ru/scripts/XML_daily.asp?date_req=" +
                format.format(current_date);
    }

    public class Enter extends AsyncTask<String, String, Boolean>{

        @Override
        protected Boolean doInBackground(String... data) {
            try {
                MediaType JSON = MediaType.get("application/json; charset=utf-8");
                User user = new User();
                user.email = data[0];
                user.password = data[1];
                Gson gson = new Gson();
                String jsonString = gson.toJson(user);
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(jsonString, JSON);
                String url = "https://reqres.in/api/";
                if (data[2]==null) url+="register";
                else url+="login";
                Request request =
                        new Request.Builder().url(new URL(url)).post(body).build();
                Response response = client.newCall(request).execute();
                Log.d("currency", response.body().string());
                return response.isSuccessful();
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean enter) {
            super.onPostExecute(enter);
            Log.d("currency", getUrl());
            if (enter){
                Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                startActivity(intent);
            }
        }
    }

    public class OkHttpHandler extends AsyncTask<Void, String, ArrayList<String>>{

        @Override
        protected ArrayList<String> doInBackground(Void... voids) {
            ArrayList<String> courses = new ArrayList<>();
            try {
                Document document = Jsoup.parse(new URL(getUrl()), 3000);
                courses.add(String.format("%.2f",
                        Double.parseDouble(
                                document.getElementById("R01235").select("Value").text().split(",")[0]+"."+
                                        document.getElementById("R01235").select("Value").text().split(",")[1])));
                courses.add(String.format("%.2f",
                        Double.parseDouble(
                                document.getElementById("R01239").select("Value").text().split(",")[0]+"."+
                                        document.getElementById("R01239").select("Value").text().split(",")[1])));
            } catch (IOException e) {
                e.printStackTrace();
            }
            return courses;
        }

        @Override
        protected void onPostExecute(ArrayList<String> courses) {
            super.onPostExecute(courses);
            String eur = "EUR " + courses.get(1);
            String usd = "USD " + courses.get(0);
            eurTV.setText(eur);
            usdTV.setText(usd);
        }
    }

}