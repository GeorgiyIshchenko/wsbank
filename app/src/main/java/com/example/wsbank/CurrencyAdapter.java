package com.example.wsbank;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CurrencyAdapter extends BaseAdapter {

    Context ctx;
    LayoutInflater inflater;
    ArrayList<CurrencyView>  currencyViews;

    public CurrencyAdapter(Context ctx,  ArrayList<CurrencyView> currencyViews) {
        this.ctx = ctx;
        this.inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.currencyViews = currencyViews;
    }

    @Override
    public int getCount() {
        return currencyViews.size();
    }

    @Override
    public Object getItem(int position) {
        return currencyViews.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.item, parent, false);
        }
        CurrencyView c = currencyViews.get(position);
        ((TextView) view.findViewById(R.id.name)).setText(c.name);
        ((TextView) view.findViewById(R.id.short_name)).setText(c.shortName);
        ((TextView) view.findViewById(R.id.buy)).setText(String.format("%.2f",c.buy));
        ((TextView) view.findViewById(R.id.sell)).setText(String.format("%.2f",c.sell));
        ((ImageView) view.findViewById(R.id.flag)).
                setImageDrawable(Drawable.createFromPath(c.shortName.substring(0, 2)+".png"));
        return view;
    }
}
