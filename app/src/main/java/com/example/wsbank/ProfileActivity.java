package com.example.wsbank;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ProfileActivity extends AppCompatActivity {

    TextView username, id, fn, ln, job;
    Button put, delete;
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        username = findViewById(R.id.username);
        fn = findViewById(R.id.first_name);
        ln = findViewById(R.id.last_name);
        image = findViewById(R.id.avatar);
        id = findViewById(R.id.user_id);
        job = findViewById(R.id.job);
        put = findViewById(R.id.put_button);
        delete = findViewById(R.id.delete_button);
        GetInfo getInfo = new GetInfo();
        getInfo.execute();
        put.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PutInfo putInfo = new PutInfo();
                putInfo.execute();
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeleteInfo deleteInfo = new DeleteInfo();
                deleteInfo.execute();
            }
        });
    }

    public class GetInfo extends AsyncTask<Void, String[], String>{

        @Override
        protected String doInBackground(Void... voids) {
            try {
                OkHttpClient client = new OkHttpClient();
                String url = "https://reqres.in/api/users/4";
                Request request =
                        new Request.Builder().url(new URL(url)).build();
                Response response = client.newCall(request).execute();
                return response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d("currency", s);
            try {
                JSONObject obj = new JSONObject(s);
                JSONObject data = obj.getJSONObject("data");
                username.setText(data.getString("email"));
                fn.setText(data.getString("first_name"));
                ln.setText(data.getString("last_name"));
                Picasso.with(ProfileActivity.this).load(data.getString("avatar")).into(image);
                String id_string = "id: "+String.valueOf(data.optInt("id"));
                id.setText(id_string);
                Log.d("currency", String.valueOf(data.optInt("id")));
            } catch (Throwable tx) {
                Log.e("currency", "json failed");
            }
        }
    }

    public class PutInfo extends AsyncTask<String, String[], String>{

        @Override
        protected String doInBackground(String... data) {
            try {
                MediaType JSON = MediaType.get("application/json; charset=utf-8");
                User user = new User();
                user.name = fn.getText().toString();
                user.job = job.getText().toString();
                Gson gson = new Gson();
                String jsonString = gson.toJson(user);
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(jsonString, JSON);
                String url = "https://reqres.in/api/users/"+id.getText().toString();
                Request request =
                        new Request.Builder().url(new URL(url)).put(body).build();
                Response response = client.newCall(request).execute();
                return response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d("currency", s);
            try {
                String data = new JSONObject(s).getString("updatedAt");
                Toast.makeText(ProfileActivity.this, data, Toast.LENGTH_SHORT).show();
            } catch (Throwable tx) {
                Log.e("currency", "json failed");
            }
        }
    }

    public class DeleteInfo extends AsyncTask<Void, String[], String>{

        @Override
        protected String doInBackground(Void... voids) {
            try {
                OkHttpClient client = new OkHttpClient();
                String url = "https://reqres.in/api/users/"+id.getText().toString();
                Request request =
                        new Request.Builder().url(new URL(url)).delete().build();
                Response response = client.newCall(request).execute();
                return response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d("currency", s);
            Toast.makeText(ProfileActivity.this, "deleted", Toast.LENGTH_SHORT).show();
        }
    }

}