package com.example.wsbank;

public class CurrencyView {

    String name;
    String shortName;
    String flag;
    Integer numCode;
    Integer nominal;
    Double buy;
    Double sell;

    public CurrencyView(String name, String shortName, String flag, Integer numCode, Integer nominal, Double buy, Double sell) {
        this.name = name;
        this.shortName = shortName;
        this.flag = flag;
        this.numCode = numCode;
        this.nominal = nominal;
        this.buy = buy;
        this.sell = sell;
    }

    public CurrencyView() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public void setNumCode(Integer numCode) {
        this.numCode = numCode;
    }

    public void setNominal(Integer nominal) {
        this.nominal = nominal;
    }

    public void setBuy(Double buy) {
        this.buy = buy;
    }

    public void setSell(Double sell) {
        this.sell = sell;
    }

}
