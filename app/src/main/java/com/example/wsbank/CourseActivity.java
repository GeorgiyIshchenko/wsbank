package com.example.wsbank;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.OkHttpClient;

public class CourseActivity extends AppCompatActivity {

    ListView list;
    TextView dateTV;
    CurrencyAdapter currencyAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course);
        dateTV = findViewById(R.id.date);
        list = findViewById(R.id.list);
        dateTV.setText(getCurrentDate());
        OkHttpHandler okHttpHandler = new OkHttpHandler();
        okHttpHandler.execute();
    }

    public String getCurrentDate(){
        Date current_date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        return format.format(current_date);
    }

    public String getUrl(){
        Date current_date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return "https://www.cbr.ru/scripts/XML_daily.asp?date_req=" +
                format.format(current_date);
    }

    public class OkHttpHandler extends AsyncTask<Void, String, ArrayList<CurrencyView>> {

        OkHttpClient client = new OkHttpClient();

        @Override
        protected ArrayList<CurrencyView> doInBackground(Void... voids) {
            ArrayList<CurrencyView> values = new ArrayList<>();
            try {
                Document document = Jsoup.parse(new URL(getUrl()), 3000);
                Elements elements = document.select("Valute");
                int nominal = 0;
                double value = 0;
                for (Element element:elements){
                    CurrencyView view = new CurrencyView();
                    view.setName(element.select("Name").text());
                    view.setShortName(element.select("CharCode").text());
                    view.setNumCode(Integer.valueOf(element.select("NumCode").text()));
                    nominal = Integer.valueOf(element.select("nominal").text());
                    view.setNominal(nominal);
                    value = Double.parseDouble(
                            document.getElementById("R01235").select("Value").text().split(",")[0]+"."+
                                    document.getElementById("R01235").select("Value").text().split(",")[1]);
                    view.setBuy(value*nominal);
                    view.setSell(value*nominal);
                    values.add(view);
                    Log.d("currency", view.name);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return values;
        }

        @Override
        protected void onPostExecute(ArrayList<CurrencyView> values) {
            super.onPostExecute(values);
            currencyAdapter = new CurrencyAdapter(CourseActivity.this, values);
            list.setAdapter(currencyAdapter);
        }
    }
}