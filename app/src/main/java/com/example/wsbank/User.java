package com.example.wsbank;

public class User {

    String email;
    String password;
    String name;
    String job;

    public User(String email, String password, String name, String job) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.job = job;
    }

    public User() {
    }
}
